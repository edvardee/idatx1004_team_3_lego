#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile


# This program requires LEGO EV3 MicroPython v2.0 or higher.
# Click "Open user guide" on the EV3 extension tab for more information.


# Create your objects here.
ev3 = EV3Brick()


# Write your program here.
ev3.speaker.beep()

# Initialize the motors.
left_motor = Motor(Port.B)
right_motor = Motor(Port.C)

speed = 100

# Initialize the drive base.
robot = DriveBase(left_motor, right_motor, wheel_diameter=55.5, axle_track=104)

sensor = UltrasonicSensor(Port.S1)
button = TouchSensor(Port.S4)

run = False
while True:
    if button.pressed():
        run = not run
        wait(100)
    while run: 
        if sensor.distance() <= 100:
            robot.stop()
            robot.straight(-100)
            robot.turn(30)
        else:
            robot.drive(speed, 0)
        if button.pressed():
            robot.stop()
            run = not run
            wait(100)
