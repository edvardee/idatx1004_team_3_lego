from ev3dev2.motor import LargeMotor, OUTPUT_A, OUTPUT_D
from ev3dev2.sensor.lego import ColorSensor
from ev3dev2.sound import Sound

# Initialize the motors and sensors
left_motor = LargeMotor(OUTPUT_A)
right_motor = LargeMotor(OUTPUT_D)
color_sensor = ColorSensor()

# Set the speed of the motors
left_motor_speed = 300
right_motor_speed = 300

# Set the threshold for detecting the black line
black_threshold = 30

# Set the proportional gain for the PID controller
Kp = 1.2

# Initialize the error and previous_error variables
error = 0
previous_error = 0

# Initialize the Sound object
sound = Sound()

# Start the motors
left_motor.on(left_motor_speed)
right_motor.on(right_motor_speed)

# Loop forever
while True:
    # Read the color sensor value
    color_value = color_sensor.reflected_light_intensity

    # Calculate the error
    error = black_threshold - color_value

    # Calculate the correction value using a PID controller
    correction = Kp * error + (Kp * (error - previous_error))

    # Update the previous_error variable
    previous_error = error

    # Adjust the motor speeds based on the correction value
    left_motor_speed = left_motor_speed + correction
    right_motor_speed = right_motor_speed - correction

    # Set the motor speeds
    left_motor.on(left_motor_speed)
    right_motor.on(right_motor_speed)

    # Play a sound to indicate that the program is running
    sound.beep()
