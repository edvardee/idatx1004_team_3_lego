#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile


# This program requires LEGO EV3 MicroPython v2.0 or higher.
# Click "Open user guide" on the EV3 extension tab for more information.


# Create your objects here.
ev3 = EV3Brick()


# Write your program here.
ev3.speaker.beep()

# Initialize the motors.
leftMotor = Motor(Port.A)
rightMotor = Motor(Port.D)
button = TouchSensor(Port.S1)

speed = 1050
def runMotors():
    leftMotor.run(speed)
    rightMotor.run(speed)

def stopMotors():
    leftMotor.stop()
    rightMotor.stop()

run = False
while True:
    if button.pressed():
        run = not run
        wait(100)
    while run:
        runMotors()
        if button.pressed():
            stopMotors()
            run = not run
            wait(100)
        
